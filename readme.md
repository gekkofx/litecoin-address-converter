# Litecoin address converter

Litecoin address converter is a simple PHP class that helps convert Litecoin addresses from M-version to 3-version and vice versa

## Dependencies
 - [base58php](https://github.com/stephen-hill/base58php) - Base58 Encoding and Decoding Library for PHP


## Installation

 1. Clone repository
```
git clone https://bitbucket.org/gekkofx/litecoin-address-converter.git
```
 2. Install dependencies
```
composer install
```
3. You are ready to go

## Usage

You can convert Litecoin address by calling

```php
LitecoinConverter :: convert( $address ) 
```

There's no need to specify version of address - it'll be determined automatically.

## Example
Check `index.php` for full example.