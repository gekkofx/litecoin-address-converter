<?php

require_once( 'vendor/autoload.php' );

class LitecoinConverter
{
    static public function convert( string $address ):string
    {
        $base58 = new \StephenHill\Base58;

        $address = bin2hex( $base58 -> decode( $address ) );
        $convertTo = substr( $address, 0, 2 ) == '05' ? '32' : '05';

        $payload = $convertTo . substr( $address, 2, -8 );
        $checksum = hash( 'sha256', hash( 'sha256', hex2bin( $payload ), true ) );
        $payload = hex2bin( $payload . substr( $checksum, 0, 8 ) );

        return $base58 -> encode( $payload );
    }
}
